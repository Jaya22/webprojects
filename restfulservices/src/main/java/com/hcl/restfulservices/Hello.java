package com.hcl.restfulservices;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Hello {

	@GetMapping("/hello")
	//@RequestMapping(method = RequestMethod.GET,path = "hello-world")
	public String getMessage() {
		return "hello webservices...";
		
	}

	@GetMapping("/hello-world-bean")
	//@RequestMapping(method = RequestMethod.GET,path = "hello-world")
	public HelloWorldBean helloWorldBean() {
		return new HelloWorldBean("hello world bean");
		
	}
	
}
